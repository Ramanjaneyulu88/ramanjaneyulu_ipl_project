

function fetchData() {
  fetch("../data/matches.csv")
    .then((response) => response.text())
    .then((data) => {
      let mathcesData = data.split("\n");
      console.log(mathcesData[0].split(","));
      console.log(mathcesData[1]);
      console.log(mathcesData[mathcesData.length]);
      mathcesData.shift();

      // Fetch deliveries

      fetch("../data/deliveries.csv")
        .then((response) => response.text())
        .then((data) => {
          let deliveriesData = data.split("\n");
          console.log(deliveriesData[0].split(","));
          console.log(deliveriesData[1]);
          deliveriesData.shift();

          // Results

          // 1. Mathes Per Year

          let matchesPerYearOutput = matchesPerYear(mathcesData);

          function matchesPerYearOutputGrap(matchesPerYearOutput) {
            Highcharts.chart("container1", {
              title: {
                text: "Matches Per Year",
              },

              yAxis: {
                title: {
                  text: "Number of Employees",
                },
              },

              xAxis: {
                accessibility: {
                  rangeDescription: "Range: 2010 to 2017",
                },
                categories: Object.keys(matchesPerYearOutput),
              },

              legend: {
                layout: "vertical",
                align: "right",
                verticalAlign: "middle",
              },

              plotOptions: {
                series: {
                  label: {
                    connectorAllowed: false,
                  },
                  pointStart: 2008,
                },
              },

              series: [
                {
                  name: "matchesPerYear",
                  data: Object.values(matchesPerYearOutput),
                },
              ],

              responsive: {
                rules: [
                  {
                    condition: {
                      maxWidth: 500,
                    },
                    chartOptions: {
                      legend: {
                        layout: "horizontal",
                        align: "center",
                        verticalAlign: "bottom",
                      },
                    },
                  },
                ],
              },
            });
          }

          matchesPerYearOutputGrap(matchesPerYearOutput);

          //  console.log(matchesPerYearOutput);

          // 2.  Won mathces per team per year

          let wonMatchesPerTeamPerYearOutput =
            wonMatchesPerTeamPerYear(mathcesData);

          function wonMatchesPerTeamPerYearGraph(obj) {
            let arr = [];

            for (let each of Object.keys(obj)) {
              arr.push({ name: each, data: Object.values(obj[each]) });
            }
            console.log(arr);

            Highcharts.chart("container2", {
              title: {
                text: "No.of Matches Won Per Team Per Year",
              },

              subtitle: {
                text: "Source: thesolarfoundation.com",
              },

              yAxis: {
                title: {
                  text: "Number of Employees",
                },
              },

              xAxis: {
                accessibility: {
                  rangeDescription: "Range: 2010 to 2017",
                },
              },

              legend: {
                layout: "vertical",
                align: "right",
                verticalAlign: "middle",
              },

              plotOptions: {
                series: {
                  label: {
                    connectorAllowed: false,
                  },
                  pointStart: 2010,
                },
              },

              series: arr,

              responsive: {
                rules: [
                  {
                    condition: {
                      maxWidth: 500,
                    },
                    chartOptions: {
                      legend: {
                        layout: "horizontal",
                        align: "center",
                        verticalAlign: "bottom",
                      },
                    },
                  },
                ],
              },
            });
          }

          wonMatchesPerTeamPerYearGraph(wonMatchesPerTeamPerYearOutput);

          //  console.log(wonMatchesPerTeamPerYear(mathcesData));

          // 3. Extra runs conceded per team

          let extraRunsConcededOutput = extraRunsConceded(
            mathcesData,
            deliveriesData
          );

          // console.log(extraRunsConcededOutput);
          function extraRunsConcededGraph(extraRunsConcededOutput) {
            Highcharts.chart("container3", {
              title: {
                text: "Extra Runs Conceded",
              },

              yAxis: {
                title: {
                  text: "Number of Employees",
                },
              },

              xAxis: {
                accessibility: {
                  rangeDescription: "Range: 2010 to 2017",
                },
              },

              legend: {
                layout: "vertical",
                align: "right",
                verticalAlign: "middle",
              },

              plotOptions: {
                series: {
                  label: {
                    connectorAllowed: false,
                  },
                  pointStart: 2010,
                },
              },

              series: [
                {
                  name: "Installation",
                  data: Object.values(extraRunsConcededOutput),
                },
              ],

              responsive: {
                rules: [
                  {
                    condition: {
                      maxWidth: 500,
                    },
                    chartOptions: {
                      legend: {
                        layout: "horizontal",
                        align: "center",
                        verticalAlign: "bottom",
                      },
                    },
                  },
                ],
              },
            });
          }

          extraRunsConcededGraph(extraRunsConcededOutput);
          // console.log(extraRunsConcededOutput);

          // 4. Economical Bowlers

          let findEconomicalBowlersOutput = findEconomicalBowlers(
            mathcesData,
            deliveriesData
          );

          function findEconomicalBowlersGraph(arr) {
            let obj = arr.reduce((acc, each) => {
              acc[each[0]] = each[1];
              return acc;
            });

            Highcharts.chart("container4", {
              title: {
                text: "Top 10 Economical Bowlers",
              },

              subtitle: {
                text: "Source: thesolarfoundation.com",
              },

              yAxis: {
                title: {
                  text: "Number of Employees",
                },
              },

              xAxis: {
                accessibility: {
                  rangeDescription: "Range: 2010 to 2017",
                },
              },

              legend: {
                layout: "vertical",
                align: "right",
                verticalAlign: "middle",
              },

              plotOptions: {
                series: {
                  label: {
                    connectorAllowed: false,
                  },
                  pointStart: 2010,
                },
              },

              series: [
                {
                  name: "Installation",
                  data: Object.values(obj),
                },
              ],

              responsive: {
                rules: [
                  {
                    condition: {
                      maxWidth: 500,
                    },
                    chartOptions: {
                      legend: {
                        layout: "horizontal",
                        align: "center",
                        verticalAlign: "bottom",
                      },
                    },
                  },
                ],
              },
            });
          }

          findEconomicalBowlersGraph(findEconomicalBowlersOutput);
          console.log(findEconomicalBowlersOutput);

          // Graphs
        });
    });
}

fetchData();

// Functions :
// 
//

// 1. Matches Per Year
function matchesPerYear(matchesArr) {
  let noMatches = {};
  matchesArr.map((eachMatch) => {
    let eachMatchArray = eachMatch.split(",");
    if (eachMatch) {
      if (noMatches[eachMatchArray[1]]) {
        noMatches[eachMatchArray[1]] += 1;
      } else {
        noMatches[eachMatchArray[1]] = 1;
      }
    }
  });

  return noMatches;
}

// 2. Number of matches won per team per year in IPL

function wonMatchesPerTeamPerYear(arr) {
  let wonMatchesObj = {};

  // Getting all the teams object

  arr.map((eachMatch, index, inputArr) => {
    let eachMatchArray = eachMatch.split(",");
    if (wonMatchesObj[eachMatchArray[10]]) {
      let eachWinnerObject = wonMatchesObj[eachMatchArray[10]];

      if (eachWinnerObject[eachMatchArray[1]]) {
        eachWinnerObject[eachMatchArray[1]] += 1;
      } else {
        eachWinnerObject[eachMatchArray[1]] = 1;
      }
    } else {
      wonMatchesObj[eachMatchArray[10]] = {};

      wonMatchesObj[eachMatchArray[10]][eachMatchArray[1]] = 1;
    }
  });

  return wonMatchesObj;
}

// 3. Extra runs conceded per team in the year 2016

function extraRunsConceded(mathcesData, deliveriesData) {
  let matchIds = mathcesData.reduce((acc, eachMatch) => {
    let eachMatchArray = eachMatch.split(",");
    if (eachMatchArray[1] === "2016") {
    acc[eachMatchArray[0]] = true
    }
    return acc;
  }, {});

  return deliveriesData.reduce((acc, eachDelivery) => {
    let eachDeliveryArray = eachDelivery.split(",");
    if (
      matchIds[eachDeliveryArray[0]] !== -1 &&
      eachDeliveryArray[16] > 0
    ) {
      if (acc[eachDeliveryArray[3]]) {
        acc[eachDeliveryArray[3]] += parseInt(eachDeliveryArray[16]);
      } else {
        acc[eachDeliveryArray[3]] = parseInt(eachDeliveryArray[16]);
      }
    }
    return acc;
  }, {});
}

// 4. Top 10 economical bowlers in the year 2015

function findEconomicalBowlers(mathcesData, deliveriesData) {
  let matchIds = mathcesData.reduce((acc, eachMatch) => {
    let eachMatchArray = eachMatch.split(",");
    if (eachMatchArray[1] === "2016") {
    acc[eachMatchArray[0]] = true
    }
    return acc;
  }, {});
  // Bowlers Total runs
  let bowlersObj = deliveriesData.reduce((acc, eachDelivery) => {
    let eachDeliveryArray = eachDelivery.split(",");

    if ( matchIds[eachDeliveryArray[0]]) {
      if (acc[eachDeliveryArray[8]]) {
        acc[eachDeliveryArray[8]]["total_runs"] += parseInt(
          eachDeliveryArray[17]
        );
        acc[eachDeliveryArray[8]]["total_balls"] += 1;
      } else {
        acc[eachDeliveryArray[8]] = {};

        acc[eachDeliveryArray[8]]["total_runs"] = parseInt(
          eachDeliveryArray[17]
        );
        acc[eachDeliveryArray[8]]["total_balls"] = 1;
      }
    }

    return acc;
  }, {});

  // Average

  let averageBowlers = Object.keys(bowlersObj).reduce((acc, eachBowler) => {
    let overs = bowlersObj[eachBowler]["total_balls"] / 6;
    let avg = bowlersObj[eachBowler]["total_runs"] / overs;

    acc.push([eachBowler, avg]);
    return acc;
  }, []);

  averageBowlers.sort((a, b) => a[1] - b[1]);
  console.log(averageBowlers);
  return averageBowlers.slice(0, 10);
}
